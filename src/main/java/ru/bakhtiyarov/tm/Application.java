package ru.bakhtiyarov.tm;

import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.model.TerminalCommand;
import ru.bakhtiyarov.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static void main(String... args) {
        displayWelcome();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static boolean parseArgs(final String... args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.ARG_ABOUT:
                displayAbout();
                break;
            case TerminalConst.ARG_HELP:
                displayHelp();
                break;
            case TerminalConst.ARG_VERSION:
                displayVersion();
                break;
            case TerminalConst.ARG_INFO:
                displayInfo();
                break;
            default:
                break;
        }
    }

    private static void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_INFO:
                displayInfo();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                break;
        }
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.8");
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ruslan Bakhtiyarov");
        System.out.println("E-MAIL: rusya.vay@mail.ru");
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalCommand.ABOUT);
        System.out.println(TerminalCommand.VERSION);
        System.out.println(TerminalCommand.HELP);
        System.out.println(TerminalCommand.EXIT);
    }

    private static void displayInfo() {
        System.out.println("INFO");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);

    }

    private static void exit() {
        System.exit(0);
    }

}

